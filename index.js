const express = require('express');
const app = express();

/* Requiring middleware */
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const expressSession = require('express-session');

/* Requiring custom module */
const appConstance = require('./constance');
const credentials = require('./credentials');

/* Define constance */

/* Set middleware */
app
    .use(express.static('public'))
    .use(bodyParser.urlencoded({ extended: true }))
    .use(cookieParser(credentials.cookieSecret))
    .use(expressSession({
        resave: false,
        saveUninitialized: false,
        secret: credentials.cookieSecret,
    }));
const options = {
    root: __dirname + '/public/',
};
app.use((req, res, next) => {
    res.sendFile('index.html', options);
});

app.listen(appConstance.PORT, () => console.log(`server was running in port: ${appConstance.PORT}`));