const path = require('path');

module.exports = {
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, '../public/build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
            }
        },{
            test: /\.(css|less)$/,
            use: ['style-loader', 'css-loader', 'less-loader'],
        }]
    },
    resolve: {
        extensions: ['.js', '.es', '.jsx'],
        modules: [
            './',
            path.resolve(__dirname, 'src'),
            'node_modules',
        ]
    }
};