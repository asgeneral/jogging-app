// @flow
type TPromiseArgs<something> = (resolve: (Object), reject: (Object)) => Promise<something>

type TRequestMethod = 'POST' | 'GET' | 'PUT' | 'DELETE';

type TApiHeader = {
    'x-http-override'?: TRequestMethod,
    'cors'?: boolean,
}
type TApiOptions = {
    headers: TApiHeader,
    method: 'GET' | 'POST'
}

export type { TApiOptions, TApiHeader, TPromiseArgs}