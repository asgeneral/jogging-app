// @flow
import type {TApiOptions, TPromiseArgs} from "./types/api";

const apiDefaultOptions: TApiOptions = {
    method: 'GET',
    headers: {
        'cors': false,
    },
};

class BaseApi {
    constructor() {
        console.log('constructor');
    }

    get(url: string, options: TApiOptions = apiDefaultOptions): Promise<Object> {
        return new Promise((resolve, reject): TPromiseArgs<Object> => {
            fetch(url, options)
                .then(data => resolve(data))
                .catch(e => console.log(e))
        })
    }
}

export { BaseApi }