// @flow
import React, {Component} from 'react';
import { Route, Switch } from 'react-router-dom';
import {PageNotFound} from 'modules/common/pageNotFound';
import {Auth} from 'modules/auth';

type TMainRouterProps = {

}

const somethingComponent = () => <h2>Component</h2>;

class   MainRouter extends Component<TMainRouterProps> {
    render() {
        return <Switch>
            <Route path='/auth' component={Auth}/>
            <Route component={PageNotFound}/>
        </Switch>
    }
}

export { MainRouter }