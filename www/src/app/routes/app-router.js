// @flow
import React, {Component} from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import {MainRouter} from 'app/routes/main-router';

type TAppRouterProps = {

}

class AppRouter extends Component<TAppRouterProps> {
    render() {
        return <Router>
            <MainRouter/>
        </Router>
    }
}

export { AppRouter }