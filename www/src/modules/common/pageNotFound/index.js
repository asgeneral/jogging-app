import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound = () =>
    (
        <div>
            <h3>PageNotFound</h3>
        </div>
    );

export { PageNotFound }