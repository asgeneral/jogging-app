// @flow
import React, { Component } from 'react';
import { Tabs } from 'antd';
import './style.less';
import {SignIn} from "modules/auth/containers/SignIn";

const TabPane = Tabs.TabPane;

type TAuthProps = {
}

class Auth extends Component<TAuthProps>{
    render() {
        return (
            <div className='Auth'>
                <Tabs>
                    <TabPane tab="SignIn" key='1'><SignIn/></TabPane>
                    <TabPane tab="SignUp" key='2'>SignUp</TabPane>
                </Tabs>
            </div>
        )
    }
}

export { Auth }