import React, { Component } from 'react';
import {SignInForm} from "modules/auth/components/SignInForm";

class SignIn extends Component {
    handleSubmit(e) {
        e.preventDefault();
        console.log(e.target.values);
    }
    render() {
        return <SignInForm handleSubmit={this.handleSubmit}/>
    }
}

export { SignIn }