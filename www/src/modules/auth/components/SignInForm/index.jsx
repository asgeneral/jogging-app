import React, {Component} from 'react';
import { Form, Icon, Input, Button } from 'antd'
const FormItem = Form.Item;

class SignInForm extends Component{
    render() {
        const { getFieldDecorator } = this.props.form;
        return <Form onSubmit={this.props.handleSubmit}>
            <FormItem
            >
                {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Please input your Password!' }],
                })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                )}
            </FormItem>
        </Form>
    }
}
const SignInFormWrapper = Form.create()(SignInForm);
export { SignInFormWrapper as SignInForm }