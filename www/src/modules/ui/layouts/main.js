import React from 'react';

const MainLayout = ({ children }) => {
    return (
        <div className='MainLayout'>
            <div className="header">Header</div>
            <div>{children}</div>
        </div>
    )
};

export { MainLayout }