import React from 'react';
import ReactDom from 'react-dom'
import {AppRouter} from 'app/routes/app-router';
import './style.css'

const appNode = document.getElementById('app');

ReactDom.render(<AppRouter/>, appNode);